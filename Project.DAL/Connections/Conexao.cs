﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration; 
using System.Data.Entity; 
using Project.DAL.Entities; 
using Project.DAL.Mappings;

namespace Project.DAL.Connections
{
    /// <summary>
    /// Classe para acesso ao BD através do EntityFramework
    /// </summary>
    class Conexao : DbContext
    {
        public Conexao() : base(ConfigurationManager.ConnectionStrings["aula"].ConnectionString)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ProdutoMapping());
        }

        public DbSet<Produto> Produto { get; set; }
    }
}
