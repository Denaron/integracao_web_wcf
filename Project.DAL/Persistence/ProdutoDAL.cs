﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Project.DAL.Entities;
using Project.DAL.Connections;

namespace Project.DAL.Persistence
{
    public class ProdutoDAL
    {
        public void Insert(Produto p)
        {
            using (Conexao con = new Conexao())
            {
                con.Entry(p).State = EntityState.Added;
                con.SaveChanges();
            }               
        }

        public void Update(Produto p)
        {
            using (Conexao con = new Conexao())
            {
                con.Entry(p).State = EntityState.Modified; //inserindo..
                con.SaveChanges(); //executando..
            }
        }

        public void Delete(Produto p)
        {
            using (Conexao con = new Conexao())
            {
                con.Entry(p).State = EntityState.Deleted; //inserindo..
                con.SaveChanges(); //executando..
            }
        }

        public List<Produto> FindAll()
        {
            using (Conexao con = new Conexao())
            {
                return con.Produto.ToList();
            }
        }

        public Produto FindById(int idProduto)
        {
            using (Conexao con = new Conexao())
            {
                return con.Produto.Find(idProduto);
            }
        }
    }
}
