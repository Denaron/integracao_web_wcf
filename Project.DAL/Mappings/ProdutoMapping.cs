﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.DAL.Entities; 
using System.Data.Entity.ModelConfiguration;

namespace Project.DAL.Mappings
{
    /// <summary>
    /// Classe de Mapeamento para a entidade produto
    /// </summary>
    class ProdutoMapping : EntityTypeConfiguration<Produto>
    {
        public ProdutoMapping()
        {
            ToTable("TB_PRODUTO");

            HasKey(p => new { p.IdProduto });

            Property(p => p.IdProduto).HasColumnName("IDPRODUTO_PK");

            Property(p => p.Nome).HasColumnName("NOMEPROD").HasMaxLength(50).IsRequired();

            Property(p => p.Preco).HasColumnName("PRECO").HasPrecision(18, 2).IsRequired();

            Property(p => p.Quantidade).HasColumnName("QUANTIDADE").IsRequired();

            Property(p => p.DataCompra).HasColumnName("DATACOMPRA").IsRequired();
        }
    }
}
