﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
namespace Projeto.Services.Models
{
    [DataContract]
    public class ProdutoModelEdicao
    {
        [DataMember(IsRequired = true)]
        public int IdProduto { get; set; }
        [DataMember(IsRequired = true)]
        public string Nome { get; set; }
        [DataMember(IsRequired = true)]
        public decimal Preco { get; set; }
        [DataMember(IsRequired = true)]
        public int Quantidade { get; set; }
        [DataMember(IsRequired = true)]
        public DateTime DataCompra { get; set; }
    }
}