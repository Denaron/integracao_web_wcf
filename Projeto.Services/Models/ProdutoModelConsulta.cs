﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Projeto.Services.Models
{
    [DataContract]
    public class ProdutoModelConsulta
    {
        [DataMember]
        public int IdProduto { get; set; }
        [DataMember]
        public string Nome { get; set; }
        [DataMember]
        public decimal Preco { get; set; }
        [DataMember]
        public int Quantidade { get; set; }
        [DataMember]
        public decimal Total { get; set; }
        [DataMember]
        public DateTime DataCompra { get; set; }
    }
}