﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Projeto.Services.Models;

namespace Projeto.Services.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IProdutoService" in both code and config file together.
    [ServiceContract]
    public interface IProdutoService
    {
        [OperationContract]
        string Cadastrar(ProdutoModelCadastro model);

        [OperationContract]
        string Atualizar(ProdutoModelEdicao model);

        [OperationContract]
        string Remover(int idProduto);

        [OperationContract]
        List<ProdutoModelConsulta> ObterTodos();

        [OperationContract]
        ProdutoModelConsulta ObterPorId(int idProduto);

    }
}
