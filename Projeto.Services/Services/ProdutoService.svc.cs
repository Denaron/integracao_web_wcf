﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Projeto.Services.Models; //camada de modelo..
using Project.DAL.Entities;
using Project.DAL.Persistence;

namespace Projeto.Services.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ProdutoService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ProdutoService.svc or ProdutoService.svc.cs at the Solution Explorer and start debugging.
    public class ProdutoService : IProdutoService
    {
        public string Atualizar(ProdutoModelEdicao model)
        {
            try
            {
                Produto p = new Produto();
                p.IdProduto = model.IdProduto;
                p.Nome = model.Nome;
                p.Preco = model.Preco;
                p.Quantidade = model.Quantidade;
                p.DataCompra = model.DataCompra;

                ProdutoDAL d = new ProdutoDAL();
                d.Update(p);

                return "Produto " + p.Nome + ", atualizado com sucesso.";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string Cadastrar(ProdutoModelCadastro model)
        {
            try
            {
                Produto p = new Produto();
                p.Nome = model.Nome;
                p.Preco = model.Preco;
                p.Quantidade = model.Quantidade;
                p.DataCompra = model.DataCompra;

                ProdutoDAL d = new ProdutoDAL();
                d.Insert(p);

                return "Produto " + p.Nome + ", cadastrado com sucesso.";

            }
            catch (Exception e)
            {
                return e.Message;
            }
        }


        public ProdutoModelConsulta ObterPorId(int idProduto)
        {
            try
            {
                ProdutoDAL d = new ProdutoDAL();
                Produto p = d.FindById(idProduto);

                if (p != null)
                {
                    ProdutoModelConsulta model = new ProdutoModelConsulta();

                    model.IdProduto = p.IdProduto;
                    model.Nome = p.Nome;
                    model.Preco = p.Preco;
                    model.Quantidade = p.Quantidade;
                    model.Total = p.Preco * p.Quantidade;
                    model.DataCompra = p.DataCompra;

                    return model;
                }
                else
                {
                    throw new Exception("Produto não encontrado.");
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }  
        }
    

        public List<ProdutoModelConsulta> ObterTodos()
        {
            try
            {
                ProdutoDAL d = new ProdutoDAL();

                List<ProdutoModelConsulta> lista = new List<ProdutoModelConsulta>();

                foreach(Produto p in d.FindAll())
                {
                    ProdutoModelConsulta model = new ProdutoModelConsulta();
                    model.IdProduto = p.IdProduto;
                    model.Nome = p.Nome;
                    model.Preco = p.Preco;
                    model.Quantidade = p.Quantidade;
                    model.Total = p.Preco * p.Quantidade;
                    model.DataCompra = p.DataCompra;

                    lista.Add(model);
                }

                return lista;
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }

        }

        public string Remover(int idProduto)
        {
            try
            {
                ProdutoDAL d = new ProdutoDAL();
                Produto p = d.FindById(idProduto);

                if(p != null)
                {
                    d.Delete(p);

                    return "Produto" + p.Nome + ", excluido com sucesso.";
                }
                else
                {
                    throw new Exception("Produto não encontrado.");
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
